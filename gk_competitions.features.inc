<?php
/**
 * @file
 * gk_competitions.features.inc
 */

/**
 * Implements hook_default_reward_type().
 */
function gk_competitions_default_reward_type() {
  $items = array();
  $items['competition_reward'] = entity_import('reward_type', '{
    "type" : "competition_reward",
    "label" : "Competition",
    "description" : ""
  }');
  return $items;
}
